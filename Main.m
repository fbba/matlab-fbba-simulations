%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% First Create some lattices
%{
global THERING;

Nlattices=300;
RINGs=cell(Nlattices,1);
for ii=1:Nlattices
    fprintf('--------------------------------------------------------\n');
    fprintf('-------------------- Lattice nº %d----------------------\n',ii);
    fprintf('--------------------------------------------------------\n');
    tic;
    CreateLattice(1);% Magnet displacements 3 times larger
    RINGs{ii}=THERING;
    toc;
end

save('RINGs.mat','RINGs');
%}
%% Prepare the run
Nlattices=numel(RINGs);
global THERING;

cd /home/zeus/ZeusApps/Studies/BBA_Simulations;

ringfile='RING.mat'; % future lattice file
varringname='RING'; %ring variable name
ringsfile='RINGs.mat'; % future lattice file
varringsname='RINGs'; %ring variable name
setao_120;
list_bpms_q=1:120;
ati=atindex(THERING);
s=findspos(THERING,1:(numel(THERING)+1));
ao=getao;

qf=findmemberof('QUAD');
qind=[];
Qdevs={};
amplqd=[];
for ii=1:numel(qf)
    qind=[qind ati.(qf{ii})];
    Qdevs=[Qdevs; ao.(qf{ii}).DeviceName];
    amplqd=[amplqd; amp2k(qf{ii},'Setpoint',2.5,ao.(qf{ii}).DeviceList)];
end
list_quads=1:numel(amplqd);
bpmindexs=ati.BPM;
sbpm=s(ati.BPM);
[sort_qind, iqsort]=sort(qind);
squad=s(sort_qind);
Qdevs=Qdevs(iqsort,:);
amplqd=amplqd(iqsort);


nbpm=numel(sbpm);
nq=numel(squad);
dist=sbpm(:)*ones(1,nq)-ones(nbpm,1)*squad(:)';
[md, iq]=min(abs(dist),[],2);
quads_names=Qdevs(iq,:);
indexsqd=sort_qind(iq);
amplqd=amplqd(iq);
list_quads=list_quads(iq);
bpm_devices=ao.BPMx.DeviceList;
bpm_names=ao.BPMx.DeviceName;
    
% skew parameters
devsquad1=[1 3; 1 5;2 3;2 6 ;3 3;3 6;4 3; 4 5];
devsquad=repmat(devsquad1,[4 1]);
addsector=[zeros(8,2);4*ones(8,1) zeros(8,1);8*ones(8,1) zeros(8,1);12*ones(8,1) zeros(8,1)];
list_bpms=dev2elem('BPMx',devsquad+addsector);

for jj=1:Nlattices
    disp(['Case ' int2str(jj) '/' int2str(Nlattices)]);
    THERING=RINGs{jj};
    R0=measbpmresp('Struct','Physics','Model');
    Rx=R0(1,1).Data;
    Ry=R0(2,2).Data;

    [mx, icmx]=max(abs(Rx),[],2);
    [my, icmy]=max(abs(Ry),[],2);
    
    ele_hcm=ao.HCM.DeviceList(icmx,:);
    ele_vcm=ao.VCM.DeviceList(icmx,:);
    amplhs_q(:,jj)=amp2k('HCM','Setpoint',0.4,ele_hcm);
    amplvs_q(:,jj)=amp2k('VCM','Setpoint',0.4,ele_vcm);
    
    indexhcm_q(:,jj)=ao.HCM.AT.ATIndex(icmx);
    indexvcm_q(:,jj)=ao.VCM.AT.ATIndex(icmy);
   
    indexhcm(:,jj)=indexhcm_q(list_bpms,jj);
    indexvcm(:,jj)=indexvcm_q(list_bpms,jj);
 
    dev_qs=ao.QS.DeviceList;
    amplhs(:,jj)=amp2k('HCM','Setpoint',0.4,ele_hcm(list_bpms,:));
    amplvs(:,jj)=amp2k('VCM','Setpoint',0.4,ele_vcm(list_bpms,:));
    amplsks=amp2k('QS','Setpoint',2.5,dev_qs);
    indexsqs=ati.QS;
    
    fh=6.9464;
    fv=5.9537;
    fskew=4.0984/2.5;
    f_s=9957.9240636;
    nuh=fh/f_s;
    nuv=fv/f_s;
    nus=fskew/f_s;
    
end

parms_file='GlobalParameters.mat';
save(parms_file,'indexsqd','indexhcm_q','indexvcm_q','ringfile','varringname',...
    'bpmindexs','amplhs_q','amplvs_q','amplqd','nuh','nuv','nus','indexsqs',...
    'indexhcm','indexvcm','amplhs','amplvs','amplsks','list_bpms','list_bpms_q',...
    'ringsfile','varringsname','list_quads');


%% RUN calculations
parms_file='GlobalParameters.mat';

options=struct;
options.parms_file=parms_file;
options.user='zeus';                    %used to squeue only my tasks
options.Ncases=100;

options.start_at_job=14;
options.case = 'skew_fbba_AC';       % objective function handle
Simulate_BBA(options);

%%
options.case = 'quad_fbba_DC';       % objective function handle
Simulate_BBA(options);
options.case = 'quad_bba';       % objective function handle
Simulate_BBA_2(options);
options.case = 'quad_fbba_AC';       % objective function handle
Simulate_BBA_2(options);


options.case = 'skew_fbba_DC';       % objective function handle
Simulate_BBA(options);








%% plot quad results
load('GlobalParameters.mat');
load('results_quad_DC_RINGs.mat');
off_quad_DCx=1e3*squeeze(BPMcentre(:,1,:))';
off_quad_DCy=1e3*squeeze(BPMcentre(:,2,:))';

load('results_quad_AC_RINGs.mat');
iscalc=squeeze(BPMcentre(1,1,:))~=0;
disp(sum(iscalc));
off_quad_ACx=1e3*squeeze(BPMcentre(:,1,:))';
off_quad_ACy=1e3*squeeze(BPMcentre(:,2,:))';

load('results_quad_RINGs.mat');
off_quad_x=1e6*squeeze(BPMcentre(:,1,:))';
off_quad_y=1e6*squeeze(BPMcentre(:,2,:))';

nseeds=size(off_quad_DCx,1);
load('RINGs_Displacements.mat');
disp_quadx=1e6*squeeze(disp_q(1:nseeds,1,list_quads));
disp_quady=1e6*squeeze(disp_q(1:nseeds,2,list_quads));


% subplot(2,1,1);
% plot(mean(disp_quadx),'--k');hold all;
% plot(mean(off_quad_DCx-disp_quadx),'-b');hold all;
% plot(mean(off_quad_ACx(iscalc,:)-disp_quadx(iscalc,:)),'-r');
% plot(mean(off_quad_x(iscalc,:)-disp_quadx(iscalc,:)),'-g');
% ylim([-5 5]);
% xlabel('BPM number');
% ylabel('Hor.Plane Mean. [\mum] ');
% subplot(2,1,2);
% plot(mean(disp_quady),'--k');hold all;
% plot(mean(off_quad_DCy-disp_quady),'-b');hold all;
% plot(mean(off_quad_ACy(iscalc,:)-disp_quady(iscalc,:)),'-r');
% plot(mean(off_quad_y(iscalc,:)-disp_quady(iscalc,:)),'-g');
% xlabel('BPM number');
% ylim([-5 5]);
% ylabel('Vert.Plane Mean. [\mum] ');
% legend('Model','DC-Model','AC-Model','BBA-Model');
% legend('Orientation','Horizontal','Location','South')
% figure;
%%
subplot(2,1,1);
semilogy(std(off_quad_DCx-disp_quadx),'-b');hold all;
semilogy(std(off_quad_ACx(iscalc,:)-disp_quadx(iscalc,:)),'-r');
semilogy(std(off_quad_x(iscalc,:)-disp_quadx(iscalc,:)),'-g');
ylim([1e-1 1e2]);
xlabel('BPM number');
ylabel('Hor.Plane Std. Dev. [\mum] ');
legend(sprintf('DC-model %1.0f\\mum',mean(std(off_quad_DCx-disp_quadx))),...
    sprintf('AC-model %1.0f\\mum',mean(std(off_quad_ACx(iscalc,:)-disp_quadx(iscalc,:)))),...
    sprintf('BBA-model %1.0f\\mum',mean(std(off_quad_x(iscalc,:)-disp_quadx(iscalc,:)))));
legend('Orientation','Horizontal','Location','South');
subplot(2,1,2);
semilogy(std(off_quad_DCy-disp_quady),'-b');hold all;
semilogy(std(off_quad_ACy(iscalc,:)-disp_quady(iscalc,:)),'-r');
semilogy(std(off_quad_y(iscalc,:)-disp_quady(iscalc,:)),'-g');
xlabel('BPM number');
ylim([1e-1 1e2]);
ylabel('Vert.Plane Std. Dev. [\mum] ');
legend(sprintf('DC-model %1.0f\\mum',mean(std(off_quad_DCy-disp_quady))),...
    sprintf('AC-model %1.0f\\mum',mean(std(off_quad_ACy(iscalc,:)-disp_quady(iscalc,:)))),...
    sprintf('BBA-model %1.0f\\mum',mean(std(off_quad_y(iscalc,:)-disp_quady(iscalc,:)))));
legend('Orientation','Horizontal','Location','South')

SetPlotSize(20,15);
print('Quad_simul_error.png','-dpng','-r600');

% figure;
% subplot(2,1,1);
% plot(mean(disp_quadx),'--k');hold all;
% plot(mean(off_quad_ACx(iscalc,:)-off_quad_DCx(iscalc,:)),'-b');
% plot(mean(off_quad_ACx(iscalc,:)-off_quad_x(iscalc,:)),'-g');
% plot(mean(off_quad_DCx(iscalc,:)-off_quad_x(iscalc,:)),'-r');
% ylim([-5 5]);
% xlabel('BPM number');
% ylabel('Hor.Plane Mean. [\mum] ');
% subplot(2,1,2);
% plot(mean(disp_quady),'--k');hold all;
% plot(mean(off_quad_ACy(iscalc,:)-off_quad_DCy(iscalc,:)),'-b');
% plot(mean(off_quad_ACy(iscalc,:)-off_quad_y(iscalc,:)),'-g');
% plot(mean(off_quad_DCy(iscalc,:)-off_quad_y(iscalc,:)),'-r');
% xlabel('BPM number');
% ylim([-5 5]);
% ylabel('Vert.Plane Mean. [\mum] ');
% legend('Model','DC-AC','BBA-AC','BBA-DC');
% legend('Orientation','Horizontal','Location','South')

figure;
%%
subplot(2,1,1);
semilogy(std(off_quad_ACx(iscalc,:)-off_quad_DCx(iscalc,:)),'-b');hold all;
semilogy(std(off_quad_ACx(iscalc,:)-off_quad_x(iscalc,:)),'-g');
semilogy(std(off_quad_DCx(iscalc,:)-off_quad_x(iscalc,:)),'-r');
ylim([1e-1 1e2]);
xlabel('BPM number');
ylabel('Hor.Plane Std. Dev. [\mum] ');

legend(sprintf('AC-DC %1.0f\\mum',mean(std(off_quad_ACx(iscalc,:)-off_quad_DCx(iscalc,:)))),...
    sprintf('AC-BBA %1.0f\\mum',mean(std(off_quad_ACx(iscalc,:)-off_quad_x(iscalc,:)))),...
    sprintf('DC-BBA %1.0f\\mum',mean(std(off_quad_DCx(iscalc,:)-off_quad_x(iscalc,:)))));
legend('Orientation','Horizontal','Location','North');

subplot(2,1,2);
semilogy(std(off_quad_ACy(iscalc,:)-off_quad_DCy(iscalc,:)),'-b');hold all;
semilogy(std(off_quad_ACy(iscalc,:)-off_quad_y(iscalc,:)),'-g');
semilogy(std(off_quad_DCy(iscalc,:)-off_quad_y(iscalc,:)),'-r');
xlabel('BPM number');
ylim([1e-1 1e2]);
ylabel('Vert.Plane Std. Dev. [\mum] ');
legend(sprintf('AC-DC %1.0f\\mum',mean(std(off_quad_ACy(iscalc,:)-off_quad_DCy(iscalc,:)))),...
    sprintf('AC-BBA %1.0f\\mum',mean(std(off_quad_ACy(iscalc,:)-off_quad_y(iscalc,:)))),...
    sprintf('DC-BBA %1.0f\\mum',mean(std(off_quad_DCy(iscalc,:)-off_quad_y(iscalc,:)))));
legend('Orientation','Horizontal','Location','South')
SetPlotSize(20,15);
print('Quad_simul_diff.png','-dpng','-r600');

%% plot skew results
load('GlobalParameters.mat');

load('results_skew_DC_RINGs.mat');
off_skew_DCx=1e3*squeeze(BPMcentre(:,1,:))';
off_skew_DCy=1e3*squeeze(BPMcentre(:,2,:))';

load('results_skew_AC_RINGs.mat');
iscalc=squeeze(BPMcentre(1,1,:))~=0;
disp(sum(iscalc));
off_skew_ACx=1e3*squeeze(BPMcentre(:,1,:))';
off_skew_ACy=1e3*squeeze(BPMcentre(:,2,:))';

nseeds=size(off_skew_ACx,1);
load('RINGs_Displacements.mat');
disp_skewx=1e6*squeeze(disp_k(1:nseeds,1,:));
disp_skewy=1e6*squeeze(disp_k(1:nseeds,2,:));


% subplot(2,1,1);
% plot(mean(off_skew_DCx-disp_skewx),'-b');hold all;
% plot(mean(off_skew_ACx(iscalc,:)-disp_skewx(iscalc,:)),'-r');
% plot(mean(off_skew_ACx(iscalc,:)-off_skew_DCx(iscalc,:)),'-g');
% ylim([-5 5]);
% xlim([1 32]);
% xlabel('BPM next to Skew magnet');
% ylabel('Hor.Plane Mean. [\mum] ');
% subplot(2,1,2);
% plot(mean(off_skew_DCy-disp_skewy),'-b');hold all;
% plot(mean(off_skew_ACy(iscalc,:)-disp_skewy(iscalc,:)),'-r');
% plot(mean(off_skew_ACy(iscalc,:)-off_skew_DCy(iscalc,:)),'-g');
% ylim([-5 5]);
% xlim([1 32]);
% ylabel('Vert.Plane Mean. [\mum] ');
% legend('DC-Model','AC-Model','DC-AC');
% legend('Location','South','Orientation','Horizontal');
% xlabel('BPM next to Skew magnet');

figure;
subplot(2,1,1);
semilogy(std(off_skew_DCx-disp_skewx),'-b');hold all;
semilogy(std(off_skew_ACx(iscalc,:)-disp_skewx(iscalc,:)),'-r');
semilogy(std(off_skew_ACx(iscalc,:)-off_skew_DCx(iscalc,:)),'-g');
ylim([1e-1 1e2]);
xlim([1 32]);
xlabel('BPM next to Skew magnet');
ylabel('Hor.Plane Std. Dev. [\mum] ');
legend(sprintf('DC-model %1.0f\\mum',mean(std(off_skew_DCx-disp_skewx))),...
    sprintf('AC-model %1.0f\\mum',mean(std(off_skew_ACx(iscalc,:)-disp_skewx(iscalc,:)))),...
    sprintf('AC-DC %1.0f\\mum',mean(std(off_skew_ACx(iscalc,:)-off_skew_DCx(iscalc,:)))));
legend('Orientation','Horizontal','Location','South')

subplot(2,1,2);
semilogy(std(off_skew_DCy-disp_skewy),'-b');hold all;
semilogy(std(off_skew_ACy(iscalc,:)-disp_skewy(iscalc,:)),'-r');
semilogy(std(off_skew_ACy(iscalc,:)-off_skew_DCy(iscalc,:)),'-g');
ylim([1e-1 1e2]);
xlim([1 32]);
ylabel('Vert.Plane Std. Dev. [\mum] ');
legend(sprintf('DC-model %1.0f\\mum',mean(std(off_skew_DCy-disp_skewy))),...
    sprintf('AC-model %1.0f\\mum',mean(std(off_skew_ACy(iscalc,:)-disp_skewy(iscalc,:)))),...
    sprintf('AC-DC %1.0f\\mum',mean(std(off_skew_ACy(iscalc,:)-off_skew_DCy(iscalc,:)))));
legend('Orientation','Horizontal','Location','South')
xlabel('BPM next to Skew magnet');

SetPlotSize(20,15);
print('Skew_simul_error.png','-dpng','-r600');
