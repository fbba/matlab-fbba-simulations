%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function save_displacements(file)
% save_displacements(c)
% Assumptions: The set of lattices with errors have been already stored in a file.
% The function will save all the quads, dipoles, skews displacements. HCMs
% and VCMs kicks are also stored.
%   
% Inputs:
%  file: '.mat' file that contains the generated lattices.

% Outputs:
%   None

%
% Date: Jan 30, 2020
% ________________________________________

load(file);

nr=numel(RINGs);
ati=atindex(RINGs{1});
famq=findmemberof('QUAD');
index_q=[];
for ii=1:numel(famq)
    index_q=[index_q ati.(famq{ii})];
end
index_q=sort(index_q);
fams=findmemberof('SEXT');
index_s=[];
for ii=1:numel(fams)
    index_s=[index_s ati.(fams{ii})];
end
index_s=sort(index_s);
index_b=ati.BEND;
index_k=ati.QS;
index_c=ati.COR;
index_RF=ati.CAV1;
n_c=numel(index_c);
n_k=numel(index_k);
n_b=numel(index_b);
n_s=numel(index_s);
n_q=numel(index_q);

disp_q=zeros(nr,2,n_q);
disp_b=zeros(nr,2,n_b);
disp_s=zeros(nr,2,n_s);
disp_k=zeros(nr,2,n_k);
kicks=zeros(nr,2,n_c);
RF=zeros(nr,1);

for ii=1:nr
    for jj=1:n_q
        disp_q(ii,:,jj)=RINGs{ii}{index_q(jj)}.T2([1 3]);
    end
    for jj=1:n_s
        disp_s(ii,:,jj)=RINGs{ii}{index_s(jj)}.T2([1 3]);
    end
    for jj=1:n_b
        disp_b(ii,:,jj)=RINGs{ii}{index_b(jj)}.T2([1 3]);
    end
    for jj=1:n_k
        disp_k(ii,:,jj)=RINGs{ii}{index_k(jj)}.T2([1 3]);
    end
    for jj=1:n_c
        kicks(ii,:,jj)=RINGs{ii}{index_c(jj)}.KickAngle;
    end
    RF(ii)=RINGs{ii}{index_RF(1)}.Frequency;
end

save([file(1:(end-4)) '_Displacements.mat'],'disp_q','disp_b','disp_s','disp_k','kicks','RF');

end

function [disp_q, disp_b,disp_sext,disp_sk,hcm,vcm]=get_displafements(RING)

end



