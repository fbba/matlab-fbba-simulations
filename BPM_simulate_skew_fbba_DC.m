%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [centre,err]=BPM_simulate_skew_fbba_DC(inputdatafile)
% [centre,err]=BPM_simulate_quad_fbba_DC(inputdatafile)
% Assumptions: It simulates a FBBA DC changed skew. This code is meant to be compiled via mcc.
%   
% Inputs:
%  inputdatafile: '.mat' file that contains the specific parameters:
%       ringfile: file name containing the ring structure
%       varringname: name of the ring structure in ringfile
%       outfilename: file name where the results are stored
%       indexsqs: AT index of the skew quadrupole to be aligned
%       iibpm: index of the BPM to be aligned
%       indexhcm: AT index list of the HCM to be used for each BPM to be aligned
%       indexvcm: AT index list of the VCM to be used for each BPM to be aligned
%       bpmindexs: BPM list of the BPMs to be used to measure the alignment
%       amplhs: absolute maximum HCM setting change during the alignment
%       amplvs: absolute maximum VCM setting change during the alignment
%       ampls: absolute maximum skew setting change during the alignment
%       nuh: requency in [1/numberofturns] used for the HCM (later on it is scaled to speedup the simulations)
%       nuv: requency in [1/numberofturns] used for the VCM (later on it is scaled to speedup the simulations)
% Outputs:
%   centre: [2x1] bpm centre in both planes
%   err: [2x1] estimated bpm error

%
% Date: Jan 30, 2020
% ________________________________________
tic;

a=load(inputdatafile);
b=load(a.ringfile,a.varringname);
outputdatafile=a.outfilename;
THERING=b.(a.varringname);
iqs=a.indexsqs(a.iibpm);
ihcm=a.indexhcm(a.iibpm);
ivcm=a.indexvcm(a.iibpm);
bpminds=a.bpmindexs;
amplh=a.amplhs(a.iibpm);
amplv=a.amplvs(a.iibpm);
ampls=a.amplsks(a.iibpm);
nuh=a.nuh;
nuv=a.nuv;

nwavesteps=2000;
factor=200;
xa=zeros(nwavesteps,numel(bpminds));
ya=zeros(nwavesteps,numel(bpminds));
xb=zeros(nwavesteps,numel(bpminds));
yb=zeros(nwavesteps,numel(bpminds));
h0=THERING{ihcm}.KickAngle(1);
v0=THERING{ivcm}.KickAngle(2);
s0=THERING{iqs}.PolynomA(2);
phaseh0=0.123;
phasev0=0.23;

THERING{iqs}.PolynomA(2)=s0-ampls;
for ii=1:nwavesteps
    THERING{ihcm}.KickAngle(1)=h0+amplh*sin(2*pi*(ii-1)*nuh*factor+phaseh0);
    THERING{ivcm}.KickAngle(2)=v0+amplv*sin(2*pi*(ii-1)*nuv*factor+phasev0);
    t=findorbit4(THERING,0,bpminds);
    xa(ii,:)=t(1,:);
    ya(ii,:)=t(3,:);
end
THERING{iqs}.PolynomA(2)=s0+ampls;
for ii=1:nwavesteps
    THERING{ihcm}.KickAngle(1)=h0+amplh*sin(2*pi*(ii-1)*nuh*factor+phaseh0);
    THERING{ivcm}.KickAngle(2)=v0+amplv*sin(2*pi*(ii-1)*nuv*factor+phasev0);
    t=findorbit4(THERING,0,bpminds);
    xb(ii,:)=t(1,:);
    yb(ii,:)=t(3,:);
end

THERING{ihcm}.KickAngle(1)=h0;
THERING{ivcm}.KickAngle(2)=v0;
THERING{iqs}.PolynomA(2)=s0;


[Axa, Mxa]=tune_search_mproj(factor*[0 nuh nuv],xa*1e3);
[Axb, Mxb]=tune_search_mproj(factor*[0 nuh nuv],xb*1e3);
[Aya, Mya]=tune_search_mproj(factor*[0 nuv nuh],ya*1e3);
[Ayb, Myb]=tune_search_mproj(factor*[0 nuv nuh],yb*1e3);
[centre, err]=find_offsets(a.jbpm,Axa,Aya,Mxa,Mya,Axb,Ayb,Mxb,Myb);


elapsed_time=toc;

save(outputdatafile,'elapsed_time','centre','err');

end
