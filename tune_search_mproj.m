%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function varargout=tune_search_mproj(nulist,f)
% [Amax mumax f0]=tune_search_mproj(nulist,f)
%Projects amplitude on nulist frequencies, does 1 or 2 iterations 
%
%INPUTS
%
%  nulist: frequencies where to project the amplitude (no units, normalized to sampling freq.)
%  f[mxn]: Array with the tracking data to be annalized
%
%  m: number of turns
%  n:number of bpms
%
%OUTPUTS
%
%  Amax: Array with the fft back transformes amplitudes of the nmax harmonics
%  mumax: Array with phases of the nmax harmonics
%  f1: residual unfitted signal
%
% EXAMPLE:
% Example 1: calculate horizontal amplitude at tune 0.152
%
% [Am mu]=tune_search_Proj(0.152,X)
%

warning('off','MATLAB:divideByZero'); 


numargout = nargout;
nmax=numel(nulist);


f0=f;
n0=size(f0,1);
nbpm=size(f0,2);
% remove mean if 0 freq is not listed
if all(nulist~=0)
    f0=f0-ones(n0,1)*mean(f0);
    isthezero=[];
else
    isthezero=find(nulist==0);
    nfreq=numel(nulist);
end

% slowest method
% comp=repmat(exp(2*pi*1i*(0:(n0-1))'*nulist(:)'),[1 1 nbpm]);
% s=squeeze(sum(permute(repmat(f0,[1,1,nmax]),[1 3 2]).*comp));
% R=real(s);
% I=imag(s);

% second slowest method
%[R I]=cproj(nulist(:),f0);



% fastest method
s=(f0'*exp(2*pi*1i*(0:(n0-1))'*nulist(:)'))';
R=real(s);
I=imag(s);

% second fastest method
%s=exp(2*pi*1i*(0:(n0-1))'*nulist(:)')'*f0;% be careful, transpose (') also conjugates
%R=real(s);
%I=imag(s);

% third fastest method
%R=cos(2*pi*(0:(n0-1))'*nulist(:)')'*f0;
%I=sin(-2*pi*(0:(n0-1))'*nulist(:)')'*f0;


Amax0=sqrt(R.^2+I.^2)/n0;
mumax0=angle(s);

f_pu=nulist(:)*ones(1,nmax)+ones(nmax,1)*nulist(:)';
f_mi=nulist(:)*ones(1,nmax)-ones(nmax,1)*nulist(:)';
chi_pu=(1-exp(-2*pi*1i*n0*f_pu))./(1-exp(-2*pi*1i*f_pu));
chi_mi=(1-exp(-2*pi*1i*n0*f_mi))./(1-exp(-2*pi*1i*f_mi));
chi_mi(isnan(chi_mi))=n0;
chi_pu(isnan(chi_pu))=n0;
chi_sum=chi_mi+chi_pu;
chi_dif=chi_mi-chi_pu;
alpha=real(chi_sum);
beta=imag(chi_sum);
gamma=real(chi_dif);
delta=imag(chi_dif);
M=[alpha -delta; beta gamma]/2;
if isempty(isthezero)
    Ve=[R;I];
    X=M\Ve;
else
    isimagzero=nfreq+isthezero;
    M(isimagzero,:)=[];
    M(:,isimagzero)=[];
    Ve=[R;I];
    Ve(isimagzero,:)=[];
    X=M\Ve;
    X=[X(1:(isimagzero-1),:);zeros(1,nbpm);X(isimagzero:end,:)];
end
Amax=sqrt(X(1:nmax,:).^2+X((1+nmax):(2*nmax),:).^2)/2;
mumax=angle(X((1+nmax):(2*nmax),:)*1i+X(1:nmax,:));



% nu=nulist(1);
% chi=(1-exp(-1i*4*nu*pi*n0))/(1-exp(-1i*4*nu*pi));
% Rc=real(chi);
% Ic=imag(chi);
% Det=n0^2-Rc^2-Ic^2;
% Rx=(R*(n0-Rc)-I*Ic)/Det;
% Ix=(I*(n0+Rc)-R*Ic)/Det;
% X=Rx+1i*Ix;
% mumax=angle(X);
% Amax=abs(X);



if numargout==1
    varargout{1} = Amax;
elseif numargout==2
    varargout{1} = Amax;
    varargout{2} = mumax;
elseif numargout==3
    arg=repmat(2*pi*(0:(n0-1))'*nulist(:)',[1 1 nbpm])+repmat(permute(mumax,[3 1 2]),[n0 1 1]);
    f1=f0-squeeze(sum(repmat(permute(2*Amax,[3 1 2]),[n0 1 1]).*cos(arg),2));
    varargout{1} = Amax;
    varargout{2} = mumax;
    varargout{3} = f1;
elseif numargout==4
    varargout{1} = Amax;
    varargout{2} = mumax;
    varargout{3} = Amax0;
    varargout{4} = mumax0;
elseif numargout==5
    varargout{1} = Amax;
    varargout{2} = mumax;
    varargout{3} = Amax0;
    varargout{4} = mumax0;
    varargout{5} = X;
elseif numargout==6
    varargout{1} = Amax;
    varargout{2} = mumax;
    varargout{3} = I;
    varargout{4} = R;
    varargout{5} = M;
    varargout{6} = Ve;
end

warning('on','MATLAB:divideByZero'); 

end
