%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Plots

nfiles=10;
for ii=1:nfiles
    files{ii}=['test_specfile_' num2str(ii) '.mat'];
    files_data{ii}=['test_data_skew_AC_' num2str(ii) '.mat'];
end


for ii=1:nfiles
    BPM_simulate_skew_fbba_AC_save_data(files{ii});
end

%
start_at_job=14;
load('RINGs_Displacements.mat');

for ii=1:nfiles
    load(files_data{ii});
    [centre(ii,:), error(ii,:)]=find_offsets_skew_proj(a.jbpm,Ax,Ay,Mx,My);
    [centre_Andrea(ii,:),error_Andrea(ii,:)]=find_offsets_skew_proj_Andrea(a.jbpm,Ax,Ay,Mx,My);
    centre_model(ii,:)=1e3*disp_k(start_at_job,:,ii);
end

for ii=1:2
    subplot(1,2,ii);
    hold all;
    plot(centre(:,ii)-centre_model(:,ii),'-r');
    plot(centre_Andrea(:,ii)-centre_model(:,ii),'-b');
end

legend('Old Formula','All real Formula')
