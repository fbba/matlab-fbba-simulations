%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [centre,err]=BPM_simulate_quad_bba(inputdatafile)
%% Function Name: BPM_simulate_quad_bba
% [centre,err]=BPM_simulate_quad_bba(inputdatafile)
% Assumptions: This code is meant to be compiled via mcc.
%   
% Inputs:
%  inputdatafile: '.mat' file that contains the specific parameters:
%       ringfile: file name containing the ring structure
%       varringname: name of the ring structure in ringfile
%       outfilename: file name where the results are stored
%       indexsqd: index of the quadrupole to be aligned
%       iibpm: index of the BPM to be aligned
%       indexhcm_q: index list of the HCM to be used for each BPM to be aligned
%       indexvcm_q: index list of the VCM to be used for each BPM to be aligned
%       bpmindexs: BPM list of the BPMs to be used to measure the alignment
%       amplhs_q: absolute maximum HCM setting change during the alignment
%       amplvs_q: absolute maximum VCM setting change during the alignment
%       amplqd: absolute maximum quadrupole setting change during the alignment

% Outputs:
%   centre: [2x1] bpm centre in both planes
%   err: [2x1] estimated bpm error

%
% Date: Jan 30, 2020
% ________________________________________

tic;


a=load(inputdatafile);
b=load(a.ringfile,a.varringname);
THERING=b.(a.varringname);
outputdatafile=a.outfilename;
iqs=a.indexsqd(a.iibpm);
ihcm=a.indexhcm_q(a.iibpm);
ivcm=a.indexvcm_q(a.iibpm);
bpminds=a.bpmindexs;
amplh=a.amplhs_q(a.iibpm);
amplv=a.amplvs_q(a.iibpm);
ampls=a.amplqd(a.iibpm);

%%
ncmsteps=251;
nquadsteps=35;
nb=numel(bpminds);
xa=zeros(nb,ncmsteps,nquadsteps);
ya=zeros(nb,ncmsteps,nquadsteps);
xb=zeros(nb,ncmsteps,nquadsteps);
yb=zeros(nb,ncmsteps,nquadsteps);
x0=zeros(nb,ncmsteps);
y0=zeros(nb,ncmsteps);

h0=THERING{ihcm}.KickAngle(1);
v0=THERING{ivcm}.KickAngle(2);
s0=THERING{iqs}.PolynomB(2);

hcm=amplh*2*((0:(ncmsteps-1))/(ncmsteps-1)-0.5);
vcm=amplv*2*((0:(ncmsteps-1))/(ncmsteps-1)-0.5);
qval=ampls*2*((0:(nquadsteps-1))/(nquadsteps-1)-0.5);


for ii=1:ncmsteps
    THERING{ihcm}.KickAngle(1)=h0+hcm(ii);
    t=findorbit4(THERING,0,bpminds);
    x0(:,ii)=t(1,:)';
end
THERING{ihcm}.KickAngle(1)=h0;

for ii=1:ncmsteps
    THERING{ivcm}.KickAngle(2)=v0+vcm(ii);
    t=findorbit4(THERING,0,bpminds);
    y0(:,ii)=t(3,:)';
end
THERING{ivcm}.KickAngle(2)=v0;

for jj=1:nquadsteps
    THERING{iqs}.PolynomB(2)=s0+qval(jj);
    for ii=1:ncmsteps
        THERING{ihcm}.KickAngle(1)=h0+hcm(ii);
        t=findorbit4(THERING,0,bpminds);
        xa(:,ii,jj)=t(1,:)';
    end
end
THERING{ihcm}.KickAngle(1)=h0;

for jj=1:nquadsteps
    THERING{iqs}.PolynomB(2)=s0+qval(jj);
    for ii=1:ncmsteps
        THERING{ivcm}.KickAngle(2)=v0+vcm(ii);
        t=findorbit4(THERING,0,bpminds);
        ya(:,ii,jj)=t(3,:)';
    end
end

THERING{iqs}.PolynomB(2)=s0;
THERING{ivcm}.KickAngle(2)=v0;

Sx=sqrt(mean(squeeze(sum((xa-repmat(x0,[1 1 nquadsteps])).^2)),2))';
Sy=sqrt(mean(squeeze(sum((ya-repmat(y0,[1 1 nquadsteps])).^2)),2))';
    
%% Sx
[~,ix]=min(Sx);
if ix>2&&ix<(ncmsteps-2)
    c=polyfit(hcm((ix-2):(ix+2)),Sx((ix-2):(ix+2)),2);
    o=-c(2)/2/c(1);
    o_l=o;
    o_r=o;
elseif ix<=2
    Sx_r=Sx((ix+1):end);
    cm_r=hcm((ix+1):end);
    c_r=polyfit(cm_r,Sx_r,1);
    o_r=-c_r(2)/c_r(1);
    o_l=o_r;
elseif ix>=(ncmsteps-2)
    Sx_l=Sx(1:(ix-1));
    cm_l=hcm(1:(ix-1));
    c_l=polyfit(cm_l,Sx_l,1);
    o_l=-c_l(2)/c_l(1);
    o_r=o_l;
end
h=(o_l+o_r)/2;
%% Sy

[~,iy]=min(Sy);
if iy>2&&iy<(ncmsteps-2)
    c=polyfit(vcm((iy-2):(iy+2)),Sy((iy-2):(iy+2)),2);
    o=-c(2)/2/c(1);
    o_l=o;
    o_r=o;
elseif iy<=2
    Sy_r=Sy((iy+1):end);
    cm_r=vcm((iy+1):end);
    c_r=polyfit(cm_r,Sy_r,1);
    o_r=-c_r(2)/c_r(1);
    o_l=o_r;
elseif iy>=(ncmsteps-2)
    Sy_l=Sy(1:(iy-1));
    cm_l=vcm(1:(iy-1));
    c_l=polyfit(cm_l,Sy_l,1);
    o_l=-c_l(2)/c_l(1);
    o_r=o_l;
end
v=(o_l+o_r)/2;
%%


[cx,stx]=polyfit(hcm,mean(xa(a.jbpm,:,:),3),1);
[cy,sty]=polyfit(vcm,mean(ya(a.jbpm,:,:),3),1);

covx=inv(stx.R)*inv(stx.R')*stx.normr^2/stx.df;
covy=inv(sty.R)*inv(sty.R')*sty.normr^2/sty.df;

centre=[polyval(cx,h) polyval(cy,v)];
err=[sqrt(covx(1,1)*h^2+covx(2,2)) sqrt(covy(1,1)*v^2+covy(2,2))];
elapsed_time=toc;
    
save(outputdatafile,'elapsed_time','centre','err');
end
