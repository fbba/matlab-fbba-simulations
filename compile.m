%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

% add path to matching functions
addpath('/home/zeus/controlroom/at');
addpath('/home/zeus/controlroom/at/atphysics');
addpath('/home/zeus/controlroom/at/lattice');
addpath('/home/zeus/controlroom/at/simulator/element');
addpath('/home/zeus/controlroom/at/simulator/track');
rmpath /home/zeus/controlroom/applications/xml/geodise; % not supported in mcc
% 
mcc('-m','BPM_simulate_quad_bba.m','-a', '/home/zeus/controlroom/at/simulator/element/*.mexa64');

%% 
mcc('-m','BPM_simulate_skew_fbba_AC.m','-a', '/home/zeus/controlroom/at/simulator/element/*.mexa64');

%%
mcc('-m','BPM_simulate_skew_fbba_DC.m','-a', '/home/zeus/controlroom/at/simulator/element/*.mexa64');

%% 
mcc('-m','BPM_simulate_quad_fbba_AC.m','-a', '/home/zeus/controlroom/at/simulator/element/*.mexa64');

%% 
mcc('-m','BPM_simulate_quad_fbba_DC.m','-a', '/home/zeus/controlroom/at/simulator/element/*.mexa64');





