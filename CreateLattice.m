%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function CreateLattice(varargin)
% CreateLattice(extrafactor)
% Sets a new lattice with errors in THERING and calibrations in the Accelerator Object.
%   
% Inputs:
%  extrafactor: extra factor in the errors rms
% Outputs:
%   None
%
% Date: Jan 30, 2020
% ________________________________________

global THERING DxHCM DxBPM mcf_val iA Circu;
warning('off');

extrafactor=1;
if nargin>=1
    extrafactor=varargin{1};
elseif nargin>1
    error('wrong number of inputs');
end

NOlattice=true;
repeat=true;

BPMx_Gain=1.02+0.015*randn3(120,1);
BPMy_Gain=0.95+0.015*randn3(120,1);
BPM_Crunch=0.01*randn3(120,1);
BPM_Roll=0.01*randn3(120,1);
HCM_Gain=0.9+0.05*randn3(88,1);
VCM_Gain=0.9+0.03*randn3(88,1);
HCM_Roll=0.01*randn3(88,1);
VCM_Roll=0.01*randn3(88,1);

BPMxDeviceListTotal = family2dev('BPMx',0);
HCMDeviceListTotal = family2dev('HCM',0);
VCMDeviceListTotal = family2dev('VCM',0);
albainit;
ao=getao;
gt=gettwiss;
ats=atsummary;
Circu=ats.circumference;
atin=atindex(THERING);
mcf_val=mcf(THERING);
DxHCM=gt.etax(atin.COR);
DxBPM=gt.etax(atin.BPM(logical(ao.BPMx.Status)));

while repeat
    while NOlattice
        dx=extrafactor*25e-6;
        dy=extrafactor*25e-6;
        df=extrafactor*50e-6;
        dxg=extrafactor*1.5e-4;
        dyg=extrafactor*1.5e-4;
        dfg=extrafactor*0.5e-4;
        dk=1e-3;
        db=1e-3;
        a25;
        Put_Errors_ALBA_Guirder(dx,dy,df,dxg,dyg,dfg,dk,db);
        orbit=getcod;
        A = measbpmresp('Model');
        NOlattice=any(isnan(orbit(:)))||any(isnan(A(:)))||any(isinf(A(:)));
    end
    
    % correct obit
    
    correct=true;
    Climit_not=true;
    fact=0.6;
    b=[getx;gety];
    iter=0;
    while correct&&Climit_not&&iter<20
        iter=iter+1;
        
        [U,S,V] = svd(A);
        iA=invSVD(U,S,V,0.05);
        c=iA*b;
        cx=c(1:88);
        cy=c(89:end);
        hc=getpv('HCM');
        vc=getpv('VCM');
        hc_new=-fact*cx+hc;
        vc_new=-fact*cy+vc;
        repeat=any(abs(hc_new)>10)||any(abs(vc_new)>10);
        Climit_not=not(repeat);
        [hc_new, RFChange] = CheckEnergyChange(hc_new);
        steppv('RF',RFChange);
        setpv('HCM',hc_new);
        setpv('VCM',vc_new);
        b=[getx;gety];
        correct=max(abs(b))>1e-2;
        fprintf('Correction %d, orbit %1.1f microns, correctors %1.1f.\n',iter,1e3*max(abs(b)),max([max(abs(hc_new)) max(abs(vc_new))]));
        A = measbpmresp('Model');
    end
    
end

setatfield('BPMx', 'GCR', [BPMx_Gain BPMy_Gain BPM_Crunch BPM_Roll], BPMxDeviceListTotal);
setatfield('HCM', 'Roll', 0*HCM_Roll, HCMDeviceListTotal, 1, 2);
setatfield('VCM', 'Roll', 0*VCM_Roll, VCMDeviceListTotal, 1, 1);
setatfield('HCM', 'Roll', HCM_Roll, HCMDeviceListTotal, 1, 1);
setatfield('VCM', 'Roll', VCM_Roll, VCMDeviceListTotal, 1, 2);

setatfield('HCM', 'Gain', HCM_Gain, HCMDeviceListTotal, 1, 1);
setatfield('VCM', 'Gain', VCM_Gain, VCMDeviceListTotal, 1, 2);
warning('on');
end

function [ch, RFChange] = CheckEnergyChange(ch)
% Check the RF change and decides if to apply or not
global DxHCM DxBPM mcf_val iA Circu;
rf=getrf;
RFChange=rf*sum(hw2physics('HCM','Monitor',ch).*DxHCM)/Circu/2;
dc=iA(1:numel(ch),1:numel(DxBPM))*DxBPM*RFChange/mcf_val/rf;
ch=ch+dc;
end
