%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [centre,err]=BPM_simulate_skew_fbba_AC_save_data(inputdatafile)
% [centre,err]=BPM_simulate_skew_fbba_AC_save_data(inputdatafile)
% It simulates a FBBA AC changed skew (full AC).
% Assumptions: This code is meant to be compiled via mcc.
%   
% Inputs:
%  inputdatafile: '.mat' file that contains the specific parameters:
%       ringfile: file name containing the ring structure
%       varringname: name of the ring structure in ringfile
%       outfilename: file name where the results are stored
%       indexsqs: AT index of the quadrupole to be aligned
%       iibpm: index of the BPM to be aligned
%       indexhcm: AT index list of the HCM to be used for each BPM to be aligned
%       indexvcm: AT index list of the VCM to be used for each BPM to be aligned
%       bpmindexs: BPM list of the BPMs to be used to measure the alignment
%       amplhs: absolute maximum HCM setting change during the alignment
%       amplvs: absolute maximum VCM setting change during the alignment
%       ampls: absolute maximum skew setting change during the alignment
%       nuh: frequency in [1/numberofturns] used for the HCM (later on it is scaled to speedup the simulations)
%       nuv: frequency in [1/numberofturns] used for the VCM (later on it is scaled to speedup the simulations)
%       nus: frequency in [1/numberofturns] used for the skew (later on it is scaled to speedup the simulations, 
%           also it is different for every skew just to check it is freq. independent)
% Outputs:
%   centre: [2x1] bpm centre in both planes
%   err: [2x1] estimated bpm error

%
% Date: Jan 30, 2020
% ________________________________________

tic;

a=load(inputdatafile);
b=load(a.ringfile,a.varringname);
outputdatafile=a.outfilename;
THERING=b.(a.varringname);
iqs=a.indexsqs(a.iibpm);
ihcm=a.indexhcm(a.iibpm);
ivcm=a.indexvcm(a.iibpm);
bpminds=a.bpmindexs;
amplh=a.amplhs(a.iibpm);
amplv=a.amplvs(a.iibpm);
ampls=a.amplsks(a.iibpm);
nuh=a.nuh;
nuv=a.nuv;
nus=a.nus*(1+2.5*(a.iibpm-1)/10);

nwavesteps=5000;
factor=200;
xa=zeros(nwavesteps,numel(bpminds));
ya=zeros(nwavesteps,numel(bpminds));
h0=THERING{ihcm}.KickAngle(1);
v0=THERING{ivcm}.KickAngle(2);
s0=THERING{iqs}.PolynomA(2);
phaseh0=50/180*pi;
phasev0=50/180*pi;
phases0=pi/2;
iih0=1;
iiv0=1;
iis0=1;

for ii=1:nwavesteps
    THERING{ihcm}.KickAngle(1)=h0+amplh*cos(2*pi*(ii-iih0)*nuh*factor+phaseh0);
    THERING{ivcm}.KickAngle(2)=v0+amplv*cos(2*pi*(ii-iiv0)*nuv*factor+phasev0);
    THERING{iqs}.PolynomA(2)=s0+ampls*cos(2*pi*(ii-iis0)*nus*factor+phases0);
    t=findorbit4(THERING,0,bpminds);
    xa(ii,:)=t(1,:);
    ya(ii,:)=t(3,:);
end
xa=xa+1e-6*rand(nwavesteps,numel(bpminds));
ya=ya+1e-6*rand(nwavesteps,numel(bpminds));

THERING{ihcm}.KickAngle(1)=h0;
THERING{ivcm}.KickAngle(2)=v0;
THERING{iqs}.PolynomA(2)=s0;

[Ax, Mx]=tune_search_mproj(factor*[0 nuh nuv nus (nuv+nus) abs(nuv-nus)],xa*1e3);
[Ay, My]=tune_search_mproj(factor*[0 nuv nuh nus (nuh+nus) abs(nuh-nus) (nuv+nus) abs(nuv-nus)],ya*1e3);

% f_s=1e4;
% fv=factor*nuv*f_s;
% fh=factor*nuh*f_s;
% fskew=factor*nus*f_s;
% [Ax, Mx,Xrem]=tune_search_mproj(factor*[0 nuh nuv nus (nuv+nus) abs(nuv-nus)],xa*1e3);
% [Ay, My,Yrem]=tune_search_mproj(factor*[0 nuv nuh nus (nuh+nus) abs(nuh-nus)],ya*1e3);% (nuv+nus) abs(nuv-nus)],ya*1e3);
% nsamples=size(xa,1);
% dx=1e3*(xa-ones(nsamples,1)*mean(xa));
% dy=1e3*(ya-ones(nsamples,1)*mean(ya));
% win=sin(pi*(0:(nsamples-1))/nsamples)';
% fx=mean(abs(fft(dx.*(win*ones(1,120))))/2*pi/nsamples,2);
% fy=mean(abs(fft(dy.*(win*ones(1,120))))/2*pi/nsamples,2);
% fxrem=mean(abs(fft(Xrem.*(win*ones(1,120))))/2*pi/nsamples,2);
% fyrem=mean(abs(fft(Yrem.*(win*ones(1,120))))/2*pi/nsamples,2);
% figure;
% subplot(2,1,1);
% semilogy((1:(nsamples-1))/nsamples*f_s/factor,fx(2:end));hold all;
% semilogy((1:(nsamples-1))/nsamples*f_s/factor,fxrem(2:end),'-g');
% ylim([1e-11 1]);
% yl=ylim;
% plot([fv fv]/factor,yl,'-k');
% plot([fv+fskew fv+fskew]/factor,yl,'-k');
% plot([fv-fskew fv-fskew]/factor,yl,'-k');
% plot([fh+fskew fh+fskew]/factor,yl,'--r');
% plot([fh-fskew fh-fskew]/factor,yl,'--r');
% xlim([0 10]);
% text(fv/factor,exp(mean(log(yl))),' f_v');
% text(fv/factor+fskew/factor,exp(mean(log(yl))),' f_v+f_s');
% text(fv/factor-fskew/factor ,exp(mean(log(yl))),' f_v-f_s');
% text(fh/factor+fskew/factor,exp(mean(log(yl))),'\color{red} f_h+f_s');
% text(fh/factor-fskew/factor,exp(mean(log(yl))),'\color{red} f_h-f_s');
% ylabel('Hor.Plane')
% xlabel('Freq. [Hz]');
% subplot(2,1,2);
% semilogy((1:(nsamples-1))/nsamples*f_s/factor,fy(2:end));hold all;
% semilogy((1:(nsamples-1))/nsamples*f_s/factor,fyrem(2:end),'-g');
% ylim([1e-11 1]);
% yl=ylim;
% plot([fh fh]/factor,yl,'-k');
% plot([fh+fskew fh+fskew]/factor,yl,'-k');
% plot([fh-fskew fh-fskew]/factor,yl,'-k');
% plot([fv+fskew fv+fskew]/factor,yl,'--r');
% plot([fv-fskew fv-fskew]/factor,yl,'--r');
% xlim([0 10]);
% text(fh/factor,exp(mean(log(yl))),' f_h');
% text(fh/factor+fskew/factor,exp(mean(log(yl))),' f_h+f_s');
% text(fh/factor-fskew/factor ,exp(mean(log(yl))),' f_h-f_s');
% text(fv/factor+fskew/factor,exp(mean(log(yl))),'\color{red} f_v+f_s');
% text(fv/factor-fskew/factor,exp(mean(log(yl))),'\color{red} f_v-f_s');
% ylabel('Vert.Plane')
% xlabel('Freq. [Hz]');

elapsed_time=toc;
[centre, err]=find_offsets_AC(a.jbpm,Ax,Ay,Mx,My);
save(['test_data_skew_AC_' inputdatafile(end-4) '.mat'],'Ax','Ay','Mx','My','a','elapsed_time');



%save(outputdatafile,'elapsed_time','centre','err');

end
