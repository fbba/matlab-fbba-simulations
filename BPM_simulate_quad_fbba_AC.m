%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [centre,err]=BPM_simulate_quad_fbba_AC(inputdatafile)
% [centre,err]=BPM_simulate_quad_fbba_AC(inputdatafile)
% It simulates a FBBA AC changed quad (full AC).
% Assumptions: This code is meant to be compiled via mcc.
%   
% Inputs:
%  inputdatafile: '.mat' file that contains the specific parameters:
%       ringfile: file name containing the ring structure
%       varringname: name of the ring structure in ringfile
%       outfilename: file name where the results are stored
%       indexsqd: index of the quadrupole to be aligned
%       iibpm: index of the BPM to be aligned
%       indexhcm_q: index list of the HCM to be used for each BPM to be aligned
%       indexvcm_q: index list of the VCM to be used for each BPM to be aligned
%       bpmindexs: BPM list of the BPMs to be used to measure the alignment
%       amplhs_q: absolute maximum HCM setting change during the alignment
%       amplvs_q: absolute maximum VCM setting change during the alignment
%       amplqd: absolute maximum quadrupole setting change during the alignment
%       nuh: frequency in [1/numberofturns] used for the HCM (later on it is scaled to speedup the simulations)
%       nuv: frequency in [1/numberofturns] used for the VCM (later on it is scaled to speedup the simulations)
%       nus: frequency in [1/numberofturns] used for the quad (later on it is scaled to speedup the simulations)
% Outputs:
%   centre: [2x1] bpm centre in both planes
%   err: [2x1] estimated bpm error

%
% Date: Jan 30, 2020
% ________________________________________

tic;

a=load(inputdatafile);
b=load(a.ringfile,a.varringname);
outputdatafile=a.outfilename;
THERING=b.(a.varringname);
iqs=a.indexsqd(a.iibpm);
ihcm=a.indexhcm_q(a.iibpm);
ivcm=a.indexvcm_q(a.iibpm);
bpminds=a.bpmindexs;
amplh=a.amplhs_q(a.iibpm);
amplv=a.amplvs_q(a.iibpm);
ampls=a.amplqd(a.iibpm);
nuh=a.nuh;
nuv=a.nuv;
nus=a.nus;

nwavesteps=5000;
factor=200;
xa=zeros(nwavesteps,numel(bpminds));
ya=zeros(nwavesteps,numel(bpminds));
h0=THERING{ihcm}.KickAngle(1);
v0=THERING{ivcm}.KickAngle(2);
s0=THERING{iqs}.PolynomB(2);
phaseh0=0.123;
phasev0=0.23;
phases0=0.3;
for ii=1:nwavesteps
    THERING{ihcm}.KickAngle(1)=h0+amplh*sin(2*pi*(ii-1)*nuh*factor+phaseh0);
    THERING{ivcm}.KickAngle(2)=v0+amplv*sin(2*pi*(ii-1)*nuv*factor+phasev0);
    THERING{iqs}.PolynomB(2)=s0+ampls*sin(2*pi*(ii-1)*nus*factor+phases0);
    t=findorbit4(THERING,0,bpminds);
    xa(ii,:)=t(1,:);
    ya(ii,:)=t(3,:);
end

THERING{ihcm}.KickAngle(1)=h0;
THERING{ivcm}.KickAngle(2)=v0;
THERING{iqs}.PolynomB(2)=s0;


[Ax, Mx]=tune_search_mproj(factor*[0 nuh nuv nus (nuh+nus) abs(nuh-nus)],xa*1e3);
[Ay, My]=tune_search_mproj(factor*[0 nuv nuh nus (nuv+nus) abs(nuv-nus)],ya*1e3);
[centre, err]=find_offsets_AC(a.jbpm,Ax,Ay,Mx,My);


elapsed_time=toc;

save(outputdatafile,'elapsed_time','centre','err');

end
