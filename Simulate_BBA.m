%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function Simulate_BBA(options)
% Simulate_BBA(options)
% Assumptions: 
%   This function launches a series of Ncases lattices with Ntasks BPMs next to the 
%   quad or skew to be aligned.
%
% Inputs:
%  options: structure that contains:
%       routine: mcc compiled script that simulates each magnet alignment
%       magnet: either 'quad' or 'skew'
%       start_at_job (optional): start at a value diferent from 1 
%       parms_file: file containin the global parameters
% Outputs:
%   None

%
% Date: Jan 30, 2020
% ________________________________________

if strcmpi(options.case,'skew_fbba_DC')
    Ntasks=32;
    result_file='results_skew_DC.mat';
    options.routine = 'BPM_simulate_skew_fbba_DC';
    options.magnet='skew';
elseif strcmpi(options.case,'skew_fbba_AC')
    Ntasks=32;
    result_file='results_skew_AC.mat';
    options.routine = 'BPM_simulate_skew_fbba_AC';
    options.magnet='skew';
elseif strcmpi(options.case,'quad_fbba_DC')
    Ntasks=120;
    result_file='results_quad_DC.mat';
    options.routine = 'BPM_simulate_quad_fbba_DC';
    options.magnet='quad';
elseif strcmpi(options.case,'quad_fbba_AC')
    Ntasks=120;
    result_file='results_quad_AC.mat';
    options.routine = 'BPM_simulate_quad_fbba_AC';
    options.magnet='quad';
elseif strcmpi(options.case,'quad_bba')
    Ntasks=120;
    result_file='results_quad.mat';
    options.routine = 'BPM_simulate_quad_bba';
    options.magnet='quad';
else
    error('wrong options.case string');
end

parms_file=options.parms_file;
st=importdata(parms_file);
load(st.ringsfile,st.varringsname);
result_file=[result_file(1:(end-4)) '_' st.ringsfile(1:(end-4)) '.mat'];

Ncases=options.Ncases;
options.Ntasks=Ntasks;
BPMcentre=zeros(Ntasks,2,Ncases);
BPMerror=zeros(Ntasks,2,Ncases);
evalTimes=zeros(Ntasks,Ncases);
last=0;
start_at_job=1;
if isfield(options,'start_at_job')
    if options.start_at_job>1
        start_at_job=options.start_at_job;
        load(result_file,'BPMcentre','BPMerror','evalTimes');
        last=(start_at_job-1)*Ntasks;
    end
end



for jobnum=start_at_job:Ncases
    RING=RINGs{jobnum};
    fprintf('\n******* Sending job %d/%d with %d tasks *****\n',jobnum,Ncases,Ntasks)
    [BPMcentre(:,:,jobnum),BPMerror(:,:,jobnum),evalTimes(:,jobnum)] = evalJob(jobnum, RING,options);
    save(result_file,'BPMcentre','BPMerror','evalTimes');
end

end

function [BPMcentre,BPMerror,evalTimes] = evalJob(jobnum, RING,options,varargin)
% Function: [pop, evalTime] = evalJob(jobnum,options, varargin)
% Description: Evaluate single BPM objective function.
%
%    Modified: Z.Marti  Data: 2016-03-22
%*************************************************************************

tStart = tic;

MRTfolder='/mnt/hpcsoftware/MATLAB/MATLAB_Runtime/v90';
N=options.Ntasks;
routine=options.routine;
parms_file=options.parms_file;
user=options.user;

%% generate input files

st0=importdata(parms_file);
save(st0.ringfile,st0.varringname);
st=st0;
for indproc=1:N
    st.outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%d') '.mat']);
    st.iibpm=indproc;
    st.indexhcm=st0.indexhcm(:,indproc);
    st.indexvcm=st0.indexvcm(:,indproc);
    st.indexhcm_q=st0.indexhcm_q(:,indproc);
    st.indexvcm_q=st0.indexvcm_q(:,indproc);
 
    st.amplhs=st0.amplhs(:,indproc);
    st.amplvs=st0.amplvs(:,indproc);
    st.amplhs_q=st0.amplhs_q(:,indproc);
    st.amplvs_q=st0.amplvs_q(:,indproc);
    
    if strcmp(options.magnet,'skew')
        st.jbpm=st.list_bpms(indproc);
    elseif strcmp(options.magnet,'quad')
        st.jbpm=st.list_bpms_q(indproc);
    else
        error('Magnet should be quad or skew');
    end
    spfn=['specfile_' num2str(indproc,'%d') '.mat'];
    save(spfn,'-struct','st');
end

%% Send all tasks


jobid=SendTask(routine,MRTfolder,N);
% wait for job to finish
Wait4Task(user,jobid,N);
% collect tasks
[isnotdone list]=CheckTask(N);

if numel(list)==N
    error(sprintf('Only 0/%d tasks were properly executed!',N));
end
while isnotdone
    jobid=SendTask(routine,MRTfolder,list);
    Wait4Task(user,jobid,N);
    [isnotdone list]=CheckTask(N);
end

%% Load Results

BPMcentre=zeros(N,2);
BPMerror=zeros(N,2);
evalTimes=zeros(N,1);
for iproc=1:N
    load(['outfile_' num2str(iproc,'%d') '.mat'],'elapsed_time','centre','err');
    BPMcentre(iproc,:)=centre;
    BPMerror(iproc,:)=err;
    evalTimes(iproc)=elapsed_time;
end

delete('*.stdout');
delete('*.stderr');
delete('*.out');
evalTime = toc(tStart);
fprintf('Elapsed time is: %1.1fs .Parallelization gives %1.0f%% speedup...\n',evalTime,100*sum(evalTimes)/evalTime-100);

% delete spec and out files
delete('spec*.mat');
delete('out*.mat');

end


function jobid=SendTask(routine,MRTfolder,N)
fid=fopen('job_array.sl','w');
fprintf(fid,'#!/bin/bash\n');
fprintf(fid,'\n');
fprintf(fid,'#SBATCH -n 1 # un core/process \n');
fprintf(fid,'#SBATCH -t 00:06:00 #temps maxim d-hh:mm:ss\n');
fprintf(fid,'#SBATCH -p short # partition fast,short,medium,long\n');
fprintf(fid,'#SBATCH -J %s\n',routine);% job name!
counting=false;
if numel(N)==1
    fprintf(fid,'#SBATCH --array=1-%d\n',N);
elseif numel(N)>1
    list=num2str(N(1));
    for ii=2:numel(N)
        if N(ii)-N(ii-1)>1
            if counting
                list=[list '-' num2str(N(ii-1)) ',' num2str(N(ii))];
            else
                list=[list ',' num2str(N(ii))];
            end
            counting=false;
        else
            if ii==numel(N)
                list=[list '-' num2str(N(ii))];
            end
            counting=true;
        end
    end
    fprintf(fid,'#SBATCH --array=%s\n',list);
end
fprintf(fid,'#SBATCH --get-user-env\n');
fprintf(fid,'#SBATCH --export=all\n');
fprintf(fid,'\n');
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/runtime/glnxa64/\n',MRTfolder);
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/bin/glnxa64/\n',MRTfolder);
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/sys/os/glnxa64/\n',MRTfolder);
fprintf(fid,'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/sys/opengl/lib/glnxa64/\n',MRTfolder);
fprintf(fid,'export MCR_CACHE_VERBOSE=true\n');
fprintf(fid,'export MCR_INHIBIT_CTF_LOCK=1\n');
fprintf(fid,'./%s ./specfile_${SLURM_ARRAY_TASK_ID}.mat\n',routine);
fclose(fid);
% run on cluster
[~, out_batch]=system('sbatch --export=ALL job_array.sl');
text_sbatch=textscan(out_batch,'%s %s %s %d');
jobid=text_sbatch{end};

end

function Wait4Task(user,jobid,N)

pause(1);

[status out_queue]=system(sprintf('squeue -u %s -j %d -o %%i',user,jobid));% list my tasks number only
counter=10;
while numel(out_queue)>7&&not(status)
    if counter>10
        counter=0;
        text_queue=textscan(out_queue(7:end),'%d_%s');
        disp(['waiting for ' num2str(numel(text_queue{1})) '/' num2str(N) ' tasks to finish.']);
    end 
    counter=counter+1;
    [status out_queue]=system(sprintf('squeue -u %s -j %d -o %%i',user,jobid));
    pause(10);
end

end

function [isnotdone, list]=CheckTask(N)

b=dir('outfile*.mat');
finproc=length(b);
isnotdone=finproc<N;

list=1:N;
isthere=zeros(numel(b),1);
for ii=1:numel(b)
    name=b(ii).name;
    isthere(ii)=str2num(name(9:(end-4)));
end
list(isthere)=[];

if isnotdone
    warning(sprintf('Only %d/%d tasks were properly executed!',finproc,N));
end

end
